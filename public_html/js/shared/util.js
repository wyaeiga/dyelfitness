function isEmail(email) {
	var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
	return regex.test(email);
}

function hash(password){
	var hashed = 0, i, char;
	if(password.length === 0){
		return hashed;
	}	
	for(i=0; i<password.length; i++){
		char = password.charCodeAt(i);
		hashed = ((hashed<<5)-hashed)+char;
		hashed |= 0;
	}
	return hashed;
}

function showHide(toShow, toHide){
	$(toShow).show();
	$(toHide).hide();
}

function go(url){
	window.location.href = url;
}