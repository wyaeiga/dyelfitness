function loginForm($scope) {
	$scope.email = '';
	$scope.password = '';
	$scope.formOkay = 'disabled';
	$scope.submitForm = function() {
		var formData = {email: $scope.email, password: $scope.password};
		$.post('/account/login', formData, function(data) {
			if (data == 'success') {
				go('/')
			}
			else {
				$('#loginBadIndicator').show();
				$('#loginBadMessage').show();
			}
		});
	}
	$scope.$watch('email', function(newVal, oldVal) {
		if (isEmail(newVal)) {
			decideButton();
		}
		$('#loginBadIndicator').hide();
		$('#loginBadMessage').hide();
	});
	$scope.$watch('password', function(newVal, oldVal) {
		if (newVal != oldVal) {
			decideButton();
		}
		$('#loginBadIndicator').hide();
		$('#loginBadMessage').hide();
	});
	function validateForm() {
		return isEmail($scope.email) && $scope.password.length > 5;
	}
	function decideButton() {
		if (validateForm()) {
			$scope.formOkay = '';
		}
		else {
			$scope.formOkay = 'disabled';
		}
	}
}