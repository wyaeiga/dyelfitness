function signUpForm($scope) {
	$scope.email = '';
	$scope.firstName = '';
	$scope.lastName = '';
	$scope.password = '';
	$scope.passwordAgain = '';
	$scope.formOkay = 'disabled';
	$scope.submitForm = function() {
		if (validateForm()) {
			var formData = {
				email: $scope.email,
				firstName: $scope.firstName,
				lastName: $scope.lastName,
				password: $scope.password
			};
			$.post('/account/signUp', formData, function(data) {
				if (data === 'success') {
					go('/account/postSignUp.html');
				}
			});
		}
	};
	$scope.$watch('email', function(newVal, oldVal) {
		//todo: MAKE THIS SUPPORT QUERYING THE DB FOR EMAILS TO SEE IF THE EMAIL ALREADY EXISTS
		//AND THEN DIRECTLY UPDATE THE UI FROM THERE INDICATING "EMAIL ALREADY EXISTS"
		if (isEmail(newVal)) {
			if (newVal != oldVal) {
				$.post('/account/checkUser', {email: $scope.email}, function(data) {
					if (data === 'success') {
						showHide('#emailOkay','#emailBad');
						showHide('#emailNotTaken','#emailTaken');
					}
					else {
						showHide('#emailTaken','#emailNotTaken');
						showHide('#emailBad','#emailOkay');
					}
				});
			}
		}
		else {
			showHide('#emailBad','#emailOkay');
			$('#emailNotTaken').hide();
			$('#emailTaken').hide();
		}
		decideButton();
	});
	$scope.$watch('firstName', function(newVal) {
		if (newVal.length > 0) {
			showHide('#firstNameOkay','#firstNameBad');
		}
		else {
			showHide('#firstNameBad','#firstNameOkay');
		}
		decideButton();
	});
	$scope.$watch('lastName', function(newVal) {
		if (newVal.length > 0) {
			showHide('#lastNameOkay','#lastNameBad');
		}
		else {
			showHide('#lastNameBad','#lastNameOkay');
		}
		decideButton();
	});
	$scope.$watch('password', function(newVal) {
		if (newVal.length > 5) {
			showHide('#passwordOkay','#passwordBad');
		}
		else {
			showHide('#passwordBad','#passwordOkay');
		}
		if (passwordsOkay()) {
			showHide('#passwordAgainOkay','#passwordAgainBad');
		}
		else {
			showHide('#passwordAgainBad','#passwordAgainOkay');
		}
		decideButton();
	});
	$scope.$watch('passwordAgain', function() {
		if (passwordsOkay()) {
			showHide('#passwordAgainOkay','#passwordAgainBad');
		}
		else {
			showHide('#passwordAgainBad','#passwordAgainOkay');
		}
		decideButton();
	});


	function validateForm() {
		return passwordsOkay() && otherOkay();
	}
	function passwordsOkay() {
		var match = $scope.password === $scope.passwordAgain;
		var lengthOkay = $scope.password.length > 5;
		return match && lengthOkay;
	}
	function otherOkay() {
		var emailOkay = isEmail($scope.email);
		var firstNameOkay = $scope.firstName.length > 0;
		var lastNameOkay = $scope.lastName.length > 0;
		return emailOkay && firstNameOkay && lastNameOkay;
	}
	function decideButton() {
		if (validateForm()) {
			$scope.formOkay = '';
		}
		else {
			$scope.formOkay = 'disabled';
		}
	}
}
