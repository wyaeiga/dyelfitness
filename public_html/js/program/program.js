function program($scope){
	$scope.programs = null;
	$scope.programDetails = null;
	$scope.searchFilter = function(obj){
		var re = new RegExp($scope.searchText, 'i');
		return !$scope.searchText || re.test(obj.programName) || re.test(obj.FirstName) || re.test(obj.LastName);
	};
	$.get('/program/getPrograms',function(data){
		$scope.programs = data;
		$scope.$apply();
	});
	$.get('/program/getProgramInfos', function(data){
		$scope.programDetails = data;
		$scope.$apply();
	});
	$scope.viewProgram = function() {
		toggleDetails(this);
	};
	$scope.addProgram = function(id) {
		var addingWorkout = {
			programId: id
		};
		$.post('/program/addProgramForUser', addingWorkout, function(data) {
			if (data === 'success') {
				$('#'+id+'Add').html('Added!');
				$('#'+id+'Add').attr('disabled','disabled');
			}
			else {
				go('/account/login.html');
			}
		});
	};
	$scope.getInfo = function(programId){
		var programDetails = Enumerable.From($scope.programDetails)
			.Where('$.programId=='+programId)
			.ToArray();
		return programDetails;
	};
}

function programForUser($scope){
	$scope.programs = null;
	$scope.programDetails = null;
	$scope.searchFilter = function(obj){
		var re = new RegExp($scope.searchText, 'i');
		return !$scope.searchText || re.test(obj.programName) || re.test(obj.FirstName) || re.test(obj.LastName);
	};
	$.get('/program/getProgramsForUser',function(data){
		$scope.programs = data;
		$scope.$apply();
	});
	$.get('/program/getProgramInfosForUser', function(data){
		$scope.programDetails = data;
		$scope.$apply();
	});
	$scope.viewProgram = function() {
		toggleDetails(this);
	};
	$scope.removeProgram = function (id) {
		var removingWorkout = {
			programId: id
		}
		$.post('/program/removeProgramForUser',removingWorkout, function(data){
			if (data === 'success') {
				$('#'+id+'Add').html('Removed!');
				setTimeout(function(){
					$('#'+id+'Expand').hide('fast');
					$('.'+id+'Info').hide('fast');
					$('.'+id+'Name').hide('fast');
					$('.'+id+'Name').parent().hide('fast');
				},1000);
				$('#'+id+'Add').attr('disabled','disabled');
			}
			else {
				go('/account/login.html');
			}
		});
	};
	$scope.getInfo = function(programId){
		var programDetails = Enumerable.From($scope.programDetails)
			.Where('$.programId=='+programId)
			.ToArray();
		return programDetails;
	};
}

function toggleDetails(item) {
	if(isCollapsed(item)) {
		$('#'+item.program.programId+'Expand').html('-');
		$('.'+item.program.programId+'Info').show('fast');
	}
	else {
		$('#'+item.program.programId+'Expand').html('+');
		$('.'+item.program.programId+'Info').hide('fast');
	}
}

function isCollapsed(item) {
	return $('#'+item.program.programId+'Expand').html() === '+';
}