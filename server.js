var pathM = require('path');
var path = pathM.join(__dirname, '/public_html/');
var port = process.env.PORT || 8000;
var http = require('http');
http.globalAgent.maxSockets = 25;
var fs = require('fs');
var express = require('express');
var mysql = require('mysql');
var squel = require('squel');
var connString = process.env.MYSQLCONNSTR_dyelfitnessdb;
var connSettings = {
	host: connString.substring(connString.indexOf("Data Source")+12,connString.indexOf(";User Id")),
	database: connString.substring(connString.indexOf("Database")+9,connString.indexOf(";Data Source")),
	user: connString.substring(connString.indexOf("User Id")+8,connString.indexOf(";Password")),
	password: connString.substring(connString.indexOf("Password")+9)
};
var conn = mysql.createConnection(connSettings);
var app = express();
app.configure(function() {
	app.use(express.logger({stream: fs.createWriteStream('./server.log',{flags:'a'})}));
	app.use(express.compress());
	app.use(express.cookieParser());
	app.use(express.bodyParser());
	app.use(express.session({secret: 'darshan patel'}));
	app.use(app.router);
});
app.get(/^(.+)$/, function(req, res) {
	var url = req.url;
	switch (url) {
		case '/':
			res.sendfile(path + 'index.html');
			break;
		case '/shared/_navBar':
			if(authed(req)){
				res.sendfile(path + '/shared/_navBarAuth.html');
			}
			else{
				res.sendfile(path + '/shared/_navBar.html');
			}
			break;
		case '/account/logout':
			logout(req, res);
			break;
		case '/program/getPrograms':
			getPrograms(req,res);
			break;
		case '/program/getProgramInfos':
			getProgramInfos(res);
			break;
		case '/program/getProgramsForUser':
			if(authed(req)){
				getProgramsForUser(req,res);
				break;
			}
			else {
				res.redirect('/account/login.html');
				break;
			}
		case '/program/getProgramInfosForUser':
			if(authed(req)){
				getProgramInfosForUser(req,res);
				break;
			}
			else {
				res.redirect('/account/login.html');
				break;
			}
		case '/account/getUser':
			getUser(req, res);
			break;
		case '/program/myPrograms.html':
			if(authed(req)){
				res.sendfile(path + req.params[0]);
				break;
			}
			else {
				res.redirect('/account/login.html');
				break;
			}
		default:
			res.sendfile(path + req.params[0]);
	}
});
app.post(/^(.+)$/, function(req, res) {
	var url = req.url;
	switch (url) {
		case '/account/signUp':
			insertUserIntoDatabase(req, res);
			break;
		case '/account/checkUser':
			checkUserInDatabase(req, res);
			break;
		case '/account/login':
			validateUser(req, res);
			break;
		case '/program/addProgramForUser':
			if(authed(req)) {
				addProgramForUser(req,res);
				break;
			}
			else {
				res.send('fail');
				break;
			}
		case '/program/removeProgramForUser':
			if(authed(req)) {
				removeProgramForUser(req,res);
				break;
			}
			else {
				res.send('fail');
				break;
			}
		default:
			res.redirect('/');
	}
});
app.listen(port);
console.log('Server listening on port '+port+'...');

setInterval(function(){
	var sql = squel
		.select()
		.from('program')
		.limit(1)
		.toString();
	conn.query(sql,function(err,rows){
		console.log('ping');
	});
},45000)


function handleDisconnect(err){
	if(err.code === 'PROTOCOL_CONNECTION_LOST' || err.code ==='ECONNRESET') {
		handleDisconnect();
	}
	else {
		console.log('Throwing '+err.code)
		throw err;
	}
}

function handleDisconnect() {
	conn = mysql.createConnection(connSettings);
	connection.connect(function(err){
		if(err){
			console.log('error when connecting to db: ',err);
			setTimeout(handleDisonnect,2000);
		}
	});

	connection.on('error', function(err){
		console.log('db error', err);
		if(err.code === 'PROTOCOL_CONNECTION_LOST' || err.code ==='ECONNRESET') {
			console.log('Handling '+err.code)
			handleDisconnect();
		}
		else {
			throw err;
		}
	});
}

//database methods
function insertUserIntoDatabase(req, res) {
	var sql = squel
			.insert()
			.into('user')
			.set('email', req.body.email)
			.set('firstname', req.body.firstName)
			.set('lastname', req.body.lastName)
			.set('password', req.body.password)
			.toString();
	conn.query(sql, function(err, rows) {
		if (err != null) {
			console.log('Handling '+err.code); handleDisconnect(err);
			res.send('fail');
		}
		if (rows != null) {
			login(req, rows.insertId);
			res.send('success');
		}
	});
}
function checkUserInDatabase(req, res) {
	var sql = squel
			.select()
			.from('user')
			.where('email = ' + '\'' + req.body.email + '\'')
			.toString();
	conn.query(sql, function(err, rows) {
		if (err != null) {
			console.log('Handling '+err.code); handleDisconnect(err);
			res.send('fail');
		}
		if (rows != null) {
			if (rows.length === 0) {
				res.send('success');
			}
			else {
				res.send('fail');
			}
		}
	});
}
function validateUser(req, res) {
	var sql = squel
			.select()
			.from('user')
			.where('email = ' + '\'' + req.body.email
				+ '\' and password = ' + '\'' + req.body.password + '\'')
			.toString();
	conn.query(sql, function(err, rows) {
		if (err != null) {
			console.log('Handling '+err.code); handleDisconnect(err);
			res.send('fail');
		}
		if (rows != null) {
			if (rows.length === 0) {
				res.send('fail');
			}
			else {
				login(req, rows[0].UserId);
				res.send('success');
			}
		}
	});
}
function updateLastActive(userId){
	var date = new Date();
	date = date.getUTCFullYear() + '-' +
		('00' + (date.getUTCMonth()+1)).slice(-2) + '-' +
		('00' + date.getUTCDate()).slice(-2) + ' ' + 
		('00' + date.getUTCHours()).slice(-2) + ':' + 
		('00' + date.getUTCMinutes()).slice(-2) + ':' + 
		('00' + date.getUTCSeconds()).slice(-2);
	var sql = squel
			.update()
			.table('user')
			.set('lastactive',date)
			.where('userId='+userId)
			.toString();
	conn.query(sql, function(err, rows){
		if(err != null) {
			console.log('Handling '+err.code); handleDisconnect(err);
		}
		if(rows != null){
		}
	});
}
function getPrograms(req,res) {
	var whereClause = req.session.userId == null ? 'null' : req.session.userId;
	var sql = squel
		.select()
		.field('p.*')
		.field('u.FirstName')
		.field('u.LastName')
		.field('case when up.userid is not null then \'disabled\' else \'\' end as enableButton')
		.field('case when up.userid is not null then \'Already Added!\' else \'Add This!\' end as buttonText')
		.from('program','p')
		.join('user','u','p.authorId=u.userId')
		.left_join('userprogram','up','p.programid = up.programid and up.userid='+whereClause)
		.toString();
	conn.query(sql, function(err, rows){
		if(err != null) {
			console.log('Handling '+err.code); handleDisconnect(err);
		}
		if(rows != null) {
			res.json(rows);
		}
	});
}
function getProgramInfos(res){
	var sql = squel
		.select()
		.field('p.*')
		.field('e.exerciseName')
		.field('pi.sets')
		.field('pi.reps')
		.from('program','p')
		.join('programinfo','pi','p.programid = pi.programid')
		.join('exercise','e','e.exerciseid = pi.exerciseid')
		.toString();
	conn.query(sql, function(err, rows){
		if(err != null) {
			console.log('Handling '+err.code); handleDisconnect(err);
		}
		if(rows != null){
			res.json(rows);
		}
	});
}
function getProgramsForUser(req, res) {
	var sql = squel
		.select()
		.field('p.*')
		.field('u.FirstName')
		.field('u.LastName')
		.from('userprogram','up')
		.join('program','p','up.programId=p.programId')
		.join('user','u','p.authorId=u.userId')
		.where('up.userid='+req.session.userId)
		.toString();
	conn.query(sql, function(err, rows){
		if(err != null) {
			console.log('Handling '+err.code); handleDisconnect(err);
		}
		if(rows != null) {
			res.json(rows);
		}
	});
}
function getProgramInfosForUser(req, res){
	var sql = squel
		.select()
		.field('p.*')
		.field('e.exerciseName')
		.field('pi.sets')
		.field('pi.reps')
		.from('userprogram','up')
		.join('program','p','up.programid=p.programid')
		.join('programinfo','pi','p.programid = pi.programid')
		.join('exercise','e','e.exerciseid = pi.exerciseid')
		.where('up.userid='+req.session.userId)
		.toString();
	conn.query(sql, function(err, rows){
		if(err != null) {
			console.log('Handling '+err.code); handleDisconnect(err);
		}
		if(rows != null){
			res.json(rows);
		}
	});
}
function login(req, userId) {
	req.session.userId = userId;
	updateLastActive(userId);
}
function authed(req){
	return req.session.userId != null;
}
function logout(req, res) {
	req.session.destroy();
	res.redirect('/');
}
function getUser(req, res) {
	var sql = squel
		.select()
		.from('user')
		.where('userid='+req.session.userId)
		.toString();
	conn.query(sql, function(err, rows){
		if(err != null){
			console.log('Handling '+err.code); handleDisconnect(err);
		}
		if(rows != null){
			res.json(rows);
		}
	});
}
function addProgramForUser(req, res){
	var sql = squel
		.insert()
		.into('userprogram')
		.set('userid',req.session.userId)
		.set('programid',req.body.programId)
		.toString();
	conn.query(sql, function(err, rows){
		if(err != null) {
			console.log('Handling '+err.code); handleDisconnect(err);
		}
		if(rows != null){
			res.send('success');
		}
	});
}

function removeProgramForUser(req, res){
	var sql = squel
		.delete()
		.from('userprogram')
		.where('userid='+req.session.userId+' and programid='+req.body.programId)
		.toString();
	conn.query(sql, function(err, rows){
		if(err != null){
			console.log('Handling '+err.code); handleDisconnect(err);
		}
		if(rows != null){
			res.send('success');
		}
	});
}